#Waterfall
The fluid web development frontend. Flexible, durable, simple, highly customisable and environmentally-friendly. Ideal as a foundation for modern responsive websites and web apps. This documentation acts as a full set of technical notes, explaining in detail each part of the Waterfall framework. If you want to get started quickly, simply refer to the cheat sheet and online published examples.

**Some key features of Waterfall:**

- A single CSS file which you or a client can easily pick-up and use. Edit any of the CSS code and delete code you are not using. No pre-compiling required. Compress CSS code afterwards if you like.
- The distinctive 'wtf-' Waterfall selector name reduces the risk of CSS code conflicting with other code or scripts in the page. You can perform a 'search and replace' on this selector name, to replace it with your own scoped selector name if you prefer.
- CSS is already grouped and organised for ease of editing in [Espresso](http://macrabbit.com/espresso/), by MacRabbit.
- A totally fluid and flexible web development framework. Percentages replace pixels and provide beautifully responsive results without ugly stepped-breakpoints or a need to build multiple versions of the same website for different devices.
- Emphasis on hardware-accelerated CSS (instead of Javascript or Flash) reduces processing power, lowers bandwidth and creates faster, more environmentally-friendly websites which need less electricity or battery power to display. Special print CSS code reduces paper volume and ink usage, for printed pages.
- Excellent compatibility with all newer web browsers and graceful degradation for older browsers. Because in the real world, clients often still want support for IE7!
- Slots nicely into existing web publishing platforms like RapidWeaver, Wordpress, Ghost, Concrete5 and ImpressPages. Typically it can be added to an existing theme or template, without any destructive changes taking place.

##Basics

###CSS Reset
Waterfall is using the famous [Eric Meyers CSS reset](http://meyerweb.com/eric/tools/css/reset/). The purpose of the CSS reset to clear-away all the sloppy browser default styling for different HTML elements and regain some consistency in base-styling between different web browsers. It smooths things out for us and removes any nasty surprises later on. There is nothing that you need to reconfigure in this CSS reset.

###Width
Useful to use if you want to constrain the width of block elements in a page. For example, you might want to set a content container to 75% wide and a sidebar to 25% wide, to create a simple two-column layout.

Widths are available in one-percent increments, ranging from 0% to 100%. So if you wanted to constrain the width of container to 65% wide, this would be the class selector name to use in the html code:

	wtf-width-65

And presented within the context of your HTML code:

	<div id="myElement" class="wtf-width-65"></div>
	
Width is calculated as a proportion to the parent (or the screen, in instances when an element has not parent). Elements with a width applied are not automatically aligned. To align a block of content left, right or centred, you would need to use auto margins (see below for more details).

Normal CSS box-model rules apply, when using widths in Waterfall.

###Floats
Floats let you take a block of content and float it either left or right. Typically floats work well in conjunction with widths, and can be applied to both block and inline elements. You can use floats to correctly align a content and sidebar container, or simply to wrap text around an image or blockquote. Floats have lots of possible uses. These are the class selectors to use, for floating an element either left or right:

	wtf-float-left

or

	wtf-float-right

This is what the code would look like in the context of some HTML:

	<div id="myElement" class="wtf-float-left"></div>

Important notes when working with floats:
 
 - There is no such thing as 'float:center' or 'float:middle' in CSS. If you want to position a block of content horizontally in the center of a parent (like a navigation menu), then use auto margins instead (see the margins chapter for more information about this).
 - If a block container only contains floating containers, you may find the parent block does not correctly wrap and enclose the floating content. 
 
In which case, simply apply a utility class of *wtf-floatfix* to the parent element, like this:

	<div id="content-container" class="wtf-floatfix">
		<div id="content" class="wtf-float-left"></div>
		<div id="sidebar" class="wtf-float-right"></div>
	</div>

This little fix will simply set the parent with an auto-height and auto-overflow, which fixes this problem nicely, with no nasty hacks or Javascript.

###Overflows
These let you adjust how content overflowing from a container is handled. By default, overflowing content will be treated as 'visible' by most web browsers. However you can change this behaviour to create special effects like borderless media or horizontal rules, and scroll boxes. Overflow can be changed using the following class selector name applied to a block element:

	wtf-overflow-[property]

The property variable shown above can be changed to one of the following:

- **hidden** to crop any overflowing content
- **visible** this is the default behavior
- **scroll** to force the display of browser scroll bars
- **auto** display of scrollbars if content overflows

In the context of HTML code, your code would look something like this:

	<div id="myElement" class="wtf-overflow-hidden"></div> 

Overflows can only be used on block elements. Some styles like scroll require that the element has a fixed size.

###Margin
Margin is the spacing applied around the outside of a block container. Margin may be applied on any block element or the horizontals of an inline element. It can be used to push elements further apart from each other, a concept commonly referred to in modern web design as 'white space'. 

Margin selector names are presented in the following format, with two basic variables...

	wtf-margin-[side]-[amount]

...where **side** is the side you want to apply margin on and **amount** is the amount of margin to be applied (between 1 and 10, or a keyword like 'auto'). 

The side variable can comprise of any of the following:

- all
- hori
- vert
- top
- right
- bottom
- left

All applies margin on all 4 sides of a container. Hori only applies margin on the left and right (horizontal) sides of a container. Vert only applies margin on the top and bottom (vertical) sides of a container. Top, right, bottom and left will only apply margin on one of those four sides of a container.

Margin amount is specified in percentage units of measurement (as a proportion to the parent container) or as keywords. Any whole number between 0 and 10 can be used for numeric margins. Typing the word 'custom' will use your custom margin amount instead. Alternatively, you may use 'auto' margins to automatically align an element left, right or centre using margins. The following are some examples of possible margin selector names that can be used:

6% of margin on all 4 sides of a container:

	wtf-margin-all-6
	
2% of padding on the left and right of a container:

	wtf-margin-hori-2
	
3% of padding on the above and below a container:

	wtf-margin-vert-3

6% of padding to the left of a container:

	wtf-margin-left-6
	
Auto left and right margins (to centre a container):

	wtf-margin-hori-auto

In the context of HTML code, a typical margin selector might look something like this, when applied to an element:

	<div id="myElement" class="wtf-margin-all-7"></div>
	
Normal CSS box-model rules apply, when using margins in Waterfall.

###Padding
Padding is the spacing applied around the inside of a block container. Padding may be applied on any block element or divisional container. It can be used to 'cushion' content and introduce space between content and the edge of a box, border or background. 

Padding selector names are presented in the following format, with two basic variables...

	wtf-padding-[side]-[amount]

...where **side** is the side you want to apply padding on and **amount** is the amount of padding to be applied (between 1 and 10). 

The side variable can comprise of any of the following:

- all
- hori
- vert
- top
- right
- bottom
- left

All applies padding on all 4 sides of a container. Hori only applies padding on the left and right sides of a container. Vert only applies padding on the top and bottom of a container. Top, right, bottom and left will only apply padding on one of those four sides of a container.

Padding is specified in percentage units of measurement, as a proportion to the parent container. Any whole number between 0 and 10 can be used. Or typing the word custom will use your custom padding amount instead. The following are some examples of possible padding selector names

6% of padding on all 4 sides of a container:

	wtf-padding-all-6
	
2% of padding on the left and right of a container:

	wtf-padding-hori-2
	
3% of padding on the above and below a container:

	wtf-padding-vert-3

6% of padding to the left of a container:

	wtf-padding-left-6
	
In the context of HTML code, a typical padding selector might look something like this, when applied to an element:

	<div id="myElement" class="wtf-padding-all-7"></div>
	
Normal CSS box-model rules apply, when using padding in Waterfall.
	
###Media Sizing
Either of these following class selector names can be applied to inline images, HTML5 video or pictures (inside figure plates):

	wtf-mediasize-upscale

or

	wtf-mediasize-downscale

Upscale will apply a width of 100% on the media, meaning that it will be stretched up beyond its original size, to fit within the given container. Downscale will constrain the media at its original size or scale it down to fit within a given container.

Both of these class selector names are optional. If neither is used, then a web browser will instead take the native sizes of the media and display it accordingly.

###Misc. Utility Classes
Also sometimes referred to as 'helper' classes. These are provided to help fix common layout issues and you may find some of these useful when working with Waterfall:

- **wtf-container** is a simple container class with positioning set to 'relative' via CSS. This makes containers ideally suited for placing absolutely positioned content inside (like logo images, buttons or badges).
- **wtf-floatfix** has been discussed a little already. To recap, this is applied on the *parent* container, which contains floating elements. This will help ensure the parent correctly wraps and encloses all floating elements contained within.
- **wtf-colfix** fixes [this common problem nicely](http://css-tricks.com/make-sure-columns-dont-collapse-horizontally/), when working with empty columns. It will basically stop empty columns (which we sometimes have perfectlylly legitimate reasons to use) from breaking a grid system and throwing things out of alignment.
- **wtf-clear-left** Used to clear floating elements from the left of a divisional box container. Does the same as the CSS 'clear:left'.
- **wtf-clear-right** Used to clear floating elements from the right of a divisional box container. Does the same as the CSS 'clear:right'.
- **wtf-clear-both** Used to clear floating elements from both sides of a divisional box container. Does the same as the CSS 'clear:both'.
- **wtf-noprint** Any elements with this class applied will be shown on the screen but not be printed.
- **wtf-noscreen** Any elements with this class applied will not be displayed on the screen, but will still get printed.
- **wtf-staging** Any elements on the page with this class applied will be set to 'display:none' via CSS. Useful if you're still drafting a page layout, but need to show the client some snapshots of development!
- **wtf-maxwidth** This is good to use in conjunction with width classes on page containers. It will stop containers sprawling to an incredibly wide width on larger screens. The default value is 1200px, and this can be changed to anything you want.

###Rounded corners
Rounded corners are achieved using the CSS *border-radius* property. Support for rounded corners is very good in all modern web browsers, and corners degrade to square corners in older web browsers like IE7 and IE8. Corners can be specified in 0.10em increments, between 0.00em and 2.00em. The class selector name to use is as follows:

	wtf-rounded-[value]

The value variable should be replaced with the amount of border radius you want to apply, using one of these values:

- **000** For 0em rounded corners
- **010** For 0.10em rounded corners
- **020** For 0.20em rounded corners
- **030** For 0.30em rounded corners
- **040** For 0.40em rounded corners
- **050** For 0.50em rounded corners
- **060** For 0.60em rounded corners
- **070** For 0.70em rounded corners
- **080** For 0.80em rounded corners
- **090** For 0.90em rounded corners
- **100** For 1.00em rounded corners
- **110** For 1.10em rounded corners
- **120** For 1.20em rounded corners
- **130** For 1.30em rounded corners
- **140** For 1.40em rounded corners
- **150** For 1.50em rounded corners
- **160** For 1.60em rounded corners
- **170** For 1.70em rounded corners
- **180** For 1.80em rounded corners
- **190** For 1.90em rounded corners
- **200** For 2.00em rounded corners


 So if you wanted to apply 0.60em rounded corners on all four sides of a container, your HTML code would resemble something like this:

	<div id="elementName" class="wtf-rounded-060">
		<!-- Code or content goes here -->
	</div>

Rounded corners are applied to all four sides of an element (which is typically the most common requirement). If you need rounded corners on individual sides of an element, you can write your own CSS code to achieve this.

##Colours
####Background fills
Waterfall uses the open source / MIT-licensed [clrs.cc](http://clrs.cc) palette of web colours. These provide a wonderful alternative to the often stark and over-used keyword CSS colours (like 'blue' and 'green' and 'red'). Background fills are flat, giving a clean, consistent and modern appearance. The class selector name to use is for background fills is as follows:

	wtf-fill-[name]

The name variable should be replaced with the name of colour fill you want to apply, using one of these values:

- **navy** For a #001F3F solid background colour fill
- **blue** For a #0074D9 solid background colour fill
- **aqua** For a #7FDBFF solid background colour fill
- **teal** For a #39CCCC solid background colour fill
- **olive** For a #3D9970 solid background colour fill
- **green** For a #2ECC40 solid background colour fill
- **lime** For a #01FF70 solid background colour fill
- **yellow** For a #FFDC00 solid background colour fill
- **orange** For a #FF851B solid background colour fill
- **red** For a #FF4136 solid background colour fill
- **maroon** For a #85144B solid background colour fill
- **fuchsia** For a #F012BE solid background colour fill
- **purple** For a #B10DC9 solid background colour fill
- **white** For a #FFFFFF solid background colour fill
- **silver** For a #DDDDDD solid background colour fill
- **gray** For a #AAAAAA solid background colour fill
- **black** For a #111111 solid background colour fill

So if you wanted to apply an aqua solid background colour fill on a divisional container, your HTML code would resemble something like this:

	<div id="elementName" class="wtf-fill-aqua">
		<!-- Code or content goes here -->
	</div>

Of course you are not restricted to just these basic colours. It would not be too difficult to design you own colour schemes and use named or numbered class selector names to apply these colours. Often the need arises to choose a single colour, and make various lighter and darker hues available for use.

For best results, use background fills that contrast text colours. Background fills can be applied to many different page elements, including (but not limited to) block containers, labels and buttons.


##Navigation Bars
####Website navigation bars
In essence, navigation bars are formed using unordered lists. If a list of pages and links contains sub-lists, then these will automatically get rendered as drop-down menu's. Getting any sort of drop-down menu to work on small devices (like mobile) is a torturous process, so on screens less than 580px wide, the drop-down menu reverts to a more simplistic toggle block navigation layout, within a scrolling box. This is the basic HTML markup used for creating a navigation bar:

	<div class="wtf-nav-wrapper">
		<div class="wtf-nav-fill"></div>
		<label for="mobile-toggle" class="mobile-toggle  wtf-navbar-300">Menu</label>
		<input type="checkbox" class="mobile-toggle" id="mobile-toggle" />
			<nav id="nav" role="navigation" class=" wtf-navbar-300">
				<ul>
					<li><a href="support/" class="normal">Homepage</a></li>
				</ul>
			</nav>
	</div><!--/wtf-nav-wrapper-->

There are a couple of important things to note about this code:

- The **wtf-nav-wrapper** is the main container that encloses the navigation links. This can have widths or margins applied to it, to space it away from other page elements.
- The **wtf-nav-fill** is a standalone container which is set to stretch to fill the entire navigation container. This makes it ideal for applying backgrounds to the navigation bar, and any applied opacity will not get inherited by other elements.

#####Navigation Bar Height
This is required in the navigation code. It performs three important tasks:

1. Tells a web browser the height at which the navigation bar should be rendered
2. Vertically aligns parent navigation link items in the navigation bar, and the toggle navigation title
3. Adjusts the point at which the vertical drop-downs meet with the horizontal parent link items

Height needs to be specified *twice*; once on the opening **wtf-nav-wrapper** and again on the **nav** tag. This will ensure both desktop and mobile navigation layouts are rendered correctly.

We specify navigation bar height and positioning in *em* units of measurement. Height can be changed using the following class selector name:

	wtf-navbar-[value]

The value variable shown above can be changed to one of the following:

- **100** For a navigation bar 1.00em tall
- **125** For a navigation bar 1.25em tall
- **150** For a navigation bar 1.50em tall
- **175** For a navigation bar 1.75em tall
- **200** For a navigation bar 2.00em tall
- **225** For a navigation bar 2.25em tall
- **250** For a navigation bar 2.50em tall
- **275** For a navigation bar 2.75em tall
- **300** For a navigation bar 3.00em tall
- **325** For a navigation bar 3.25em tall
- **350** For a navigation bar 3.50em tall
- **375** For a navigation bar 3.75em tall
- **400** For a navigation bar 4.00em tall
- **425** For a navigation bar 4.25em tall
- **450** For a navigation bar 4.50em tall
- **475** For a navigation bar 4.75em tall
- **500** For a navigation bar 5.00em tall



In the context of HTML code, your code would look something like this:

	<nav class="wtf-navbar-300"></nav>
	
The code example shown above of the complete navigation bar code shows where these height class selectors need to go. Each navigation bar needs two, of the same name.

####Navigation bar alignment
Navigation bars in Waterfall can be aligned left, right or centred. This is achieved by putting the following class selector name on the main **wtf-nav-wrapper** container:

	wtf-navbar-[value]
	
The value variable shown above can be changed to one of the following:

- **left** for left alignment of the parent navigation links
- **right** for right alignment of the parent navigation links
- **center** for center alignment of the parent navigation links


In the context of HTML code, your code would look something like this:

	<div class="wtf-nav-wrapper wtf-nav-center">
		<!-- Other navigation bar code goes here-->
	</div>


Navigation links contained in drop-downs remain unchanged, and will have an auto-width and left alignment. By default, navigation bars will be switch to left alignment, if no alignment selector is provided.